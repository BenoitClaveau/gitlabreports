# GitLab Reports
GitLab Reports is a small utility web app built to help developers (using GitLab for their projects) to generate time tracking reports for their GitLab projects. App's purpose is to provide a clean, quick and to-the-point interface to track progress of the tasks.

<a href="https://image.prntscr.com/image/iFbYh6lpQGWKbfmOsHk7wQ.png"><img src="https://image.prntscr.com/image/iFbYh6lpQGWKbfmOsHk7wQ.png" /></a>

## Features
+ After being authenticated by GitLab, developers can select desired project and then view/generate reports on the web based on select Milestone. 
+ Reports can be created based on select labels (like enhancement, bug, task etc.) and issue states (opened or closed). 
+ Developers can customize reports data by selecting what columns to show or hide.
+ GitLab Reports supports calculating Total Estimate Time and Total Time Spent of all issues including in the project report. This helps developers analyzing that how much total time they have spent on specific project milestones which helps tracking progress. 

## Using on Self-hosted GitLab Installations
Make the following modifications to these files
* **app.factory.gitlab.js**
  1. Replace all instances of `gitlab.com` with `your_gitlab_installation.com`
* **app.service.auth.js**
  1. Replace the `redirect_uri` var with your the URI of your Cosango Gitlab Reports app.
  1. Replace the `client_id` var with a new ID generated in *Profile > Applications*. You will create the application just like the following screenshot selecting API access and the proper callback URL you set in the previous step. <img src="https://i.imgur.com/DRepGAk.png" />
  1. Replace the `authorize_url` var with `your_gitlab_installation.com` instead of `gitlab.com` 

## Save as PDF
In next releases, we will add support of exporting report to different formats specially PDF along with other enhancements and bug fixes. However, users can still create reports in PDF format by clicking Print button in app's header-toolbar and then selecting **Save as PDF** in destination as shown in below screen shot:

<a href="https://image.prntscr.com/image/QNj5IzdjQqmQATdlc0fgBw.png"><img src="https://image.prntscr.com/image/QNj5IzdjQqmQATdlc0fgBw.png" /></a>

## Resources
+ **GitLab Reports App:** [http://gitlabreports.cosango.com/](http://gitlabreports.cosango.com/)
+ **Cosango Apps:** [http://cosango.com/apps/](http://cosango.com/apps/)
+ **Cosango:** [http://cosango.com/](http://cosango.com)