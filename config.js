(function () {
    'use strict';

    function config() {
        //localStorage.setItem('private_token', "JK3Q6uw9FxecsZ-t6hwR");
        
        return {
            host: function() {
                return "https://gitlab.com"; //"http://git.izzili.com"; 
            },
            callbackHost() {
                return "http://gitlabreports.cosango.com"
            },
            projectId: function () {
                return null;
            },
            label: function () {
                return null;
            },
            milestone: function () {
                return null;
            }
        };
    }

    angular.module('GitLabReportApp').service('config', config);

})();

