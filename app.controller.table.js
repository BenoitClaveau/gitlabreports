(function () {
    'use strict';

    function main($rootScope, $scope, gitlab, auth) {

        $rootScope.$on('local-storage-updated', function () {
            $scope.comulative_time_spent = 0;
            $scope.comulative_time_estimate = 0;
            $scope.loadIssues();
        });

        $scope.loadIssues = function () {
            var params = {};
            if (localStorage.getItem('private_token')) {
                params.private_token = localStorage.getItem('private_token');
            } else if (localStorage.getItem('access_token')) {
                params.access_token = localStorage.getItem('access_token');
            } else {
                return;
            }
            if (localStorage.getItem('project_id')) {
                params.id = localStorage.getItem('project_id');
            } else {
                return;
            }
            if (localStorage.getItem('state')) {
                params.state = localStorage.getItem('state');
            }
            if (localStorage.getItem('milestone')) {
                params.milestone = localStorage.getItem('milestone');
            }
            if (localStorage.getItem('labels')) {
                params.labels = localStorage.getItem('labels');
            }
            $scope.issues = gitlab.projects_issues.query(
                params,
                function () {
                },
                function () {
                    auth.redirectToOauth();
                }
            );
        };

        $scope.getIssuesTimeStats = function (issue_project_id, issue_iid) {
            var params = {
                id: issue_project_id,
                issue_iid: issue_iid
            }
            if (localStorage.getItem('private_token')) params.private_token = localStorage.getItem('private_token');
            if (localStorage.getItem('access_token')) params.access_token = localStorage.getItem('access_token');
                
            var stats = gitlab.issues_time_stats.get(
                params,
                function (res) {
                    if (stats.human_time_estimate) {
                        stats.human_time_estimate = stats.human_time_estimate
                            .replace("m", " minutes")
                            .replace("d", " jours")
                            .replace("h", " heures")
                            .replace("w", " semaines")
                            .replace("1 semaines", "1 semaine")
                            .replace("1 jours", "1 jour")
                            .replace("1 heures", "1 heure")
                            .replace("1 minutes", "1 miniute");
                    }

                    if (stats.human_total_time_spent) {
                        stats.human_total_time_spent = stats.human_total_time_spent
                            .replace("m", " minutes")
                            .replace("d", " jours")
                            .replace("h", " heures")
                            .replace("w", " semaines")
                            .replace("1 semaines", "1 semaine")
                            .replace("1 jours", "1 jour")
                            .replace("1 heures", "1 heure")
                            .replace("1 minutes", "1 miniute");
                    }

                    stats.percent_time_spent = stats.total_time_spent / stats.time_estimate * 100 + "%";

                    $scope.comulative_time_estimate += stats.time_estimate;
                    $scope.comulative_time_spent += stats.total_time_spent;
                    $scope.comulative_percent_time_spent = Math.ceil($scope.comulative_time_spent / $scope.comulative_time_estimate * 100) + "%"
                    
                }
            );

            return stats;
        };

    }
	
	

    angular.module('GitLabReportApp').controller('TableController', main);

})();

